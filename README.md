# openharmony萌新贡献指南

[TOC]

本篇教程旨在帮助萌新们一起参与到OpenHarmony的开源事业中，让每个人成为开源人，星星之火，可以燎原！最下面有我的入群方式。

活动范围涵盖OpenHarmony主干仓、SIG仓、三方库，共计1000+个代码仓：

- OpenHarmony主库组织地址：

https://gitee.com/openharmony

- OpenHarmony SIG组织地址：

https://gitee.com/openharmony-sig

-  OpenHarmony三方库组织地址：

https://gitee.com/openharmony-tpc

## 前提条件

1. 有一个[Gitee账号](https://gitee.com/)，没有的话，记得[用邮箱注册一个](https://gitee.com/signup)
2. [签署开发者原创声明](https://dco.openharmony.cn/sign)
3. [本地安装Git](https://git-scm.com/downloads)

如果Git基础薄弱，也是可以用图形化界面

### windows推荐

- [tortoisegit](https://tortoisegit.org/)  
-  [sourcetree]( https://www.sourcetreeapp.com/) 

### mac推荐

-   [sourcetree]( https://www.sourcetreeapp.com/) 

接下来就是手把手的环节了

在这之前先说一下，从我这儿报名，提交Pr，

### 有哪些福利

- 官方的T恤

- 我的鸿蒙书籍
- 我的小鸟摆件



## 一、配置Git

[注册码云账号](注册码云账号.md)

[本地安装Git](https://git-scm.com/downloads)

## 二、DCO签署

### 1.DCO签署网址

[开发者原创声明Developer Certificate of Origin](https://dco.openharmony.cn/sign)

[开发者原创声明](https://dco.openharmony.io/sign-dco)

### 2.签署DCO

![向OpenHarmony社区提交代码](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220625103528468.png)



### 3.修改本地的邮箱和签署邮箱一致

```html
git config --global user.name "徐建国" 
git config --global user.email "852851198@qq.com"
git config --global  --list
```

注意

DCO签署Name 必须要和git config --global user.name 设置保持一致（其实不一致也可以，但一致之后少麻烦）

DCO签署E-mail必须要和git config --global user.email设置保持一致

## 三、提交代码

### 1.下载代码

#### 1.1找到要提交代码的社区代码仓，fork一个到自己的私有仓中；

![image-20220625103805814](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220625103805814.png)





#### 1.2复制自己私有仓的“克隆/下载”中的HTTPS/SSH链接下载代码

本地创建工作区，然后

```
git clone https://gitee.com/jianguo888/docs.git
```

![image-20220625105125849](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220625105125849.png)



### 2.提交代码

#### 2.1将修改的代码合入到提交代码仓中；

必须添加邮箱，否则后面的doc校验不过

```
git add .
git commit -sm '修改信息+名字+邮箱' // 提交信息包含邮箱，这个邮箱是你签署开发者原创声明协议的那个邮箱
git push -f origin master 

```

比如我的：

```
git add .
git commit -s -m '修改redeme提高可读性 ' // 提交信息包含signoff邮箱
git push -f origin master 


```

切记：必须是-s -m

 -s的意思就是带了你的signoff

#### 2.2如果是对同一个问题修改

```
git commit --amend
```

通常推荐一个一个commit解决一个问题

## 四、提交ISSUE



### 1.[进入社区主代码建Issue](https://gitee.com/openharmony/docs)（不是fork的代码仓），同时注意建Issue有很多选项类型可选择，根据实际情况选择

名称

【OpenHarmony开源贡献者计划2022】+Issue内容描述

![image-20220625105723070](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220625105723070.png)



![向OpenHarmony社区提交代码-开源基础软件社区](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/resize,w_617,h_277.png)

### 2.创建成功会生成一个#XXXXX(I5E2H2)的IssueID，后续提交PR可以关联，关联PR合入，Issue就会自动关闭。

![image-20220625105939890](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220625105939890.png)

## 五、提交PR

### 1.代码提交到自己的私有仓，刷新，点击“+ Pull Request”建PR合入代码到社区主代码仓；

![image-20220625110150526](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220625110150526.png)

### 2.进入PR提交界面，可选择代码仓库分支，和关联ISSUE ID，简单描述合入的PR修改等信息；

![image-20220625110210970](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220625110210970.png)

标题都是【OpenHarmony开源贡献者计划2022】+你修改的任务摘要

比如我的：

【OpenHarmony开源贡献者计划2022】+智能家居中控

![image-20220719131555042](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220719131555042.png)





### 3.PR建立成功，首先默认进行DCO检查，检查成功，需要手动在评论区输入回复”start build”方可进入代码的CI静态检查和编译等操作。

![image-20220625110230911](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220625110230911.png)


## 六、联系committer

committer：https://gitee.com/openharmony/community/blob/master/zh/committer.md

### 1. committer文档中找到对应的committer负责人主页，想办法联系

这一步，可以找我沟通就好，我帮大家联系。

我是在committer发现负责人，然后微信搜索，群里捞到的。

![向OpenHarmony社区提交代码](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/resize,w_632,h_283.png)



![image-20220625110507371](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220625110507371.png)



## 七、我的群以及微信

### 个人微信

![account](img/account.png)

### 扫码加群

![group](img/qun.png)

## 参考资料

[贡献代码的流程](https://gitee.com/openharmony/docs/blob/master/zh-cn/contribute/%E8%B4%A1%E7%8C%AE%E4%BB%A3%E7%A0%81.md)



## 感谢徐建国 徐老师发布的文档!!!